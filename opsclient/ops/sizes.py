
sizes = {
    "zero": {
        "cinder": {
            "gigabytes": 0,
            "volumes": 0,
            "snapshots": 0
        },
        "nova": {
            "metadata_items": 0,
            "injected_file_content_bytes": 0,
            "ram": 0,
            "floating_ips": 0,
            "key_pairs": 50,
            "instances": 0,
            "server_groups": 0,
            "injected_files": 0,
            "cores": 0,
            "fixed_ips": 0,
        },
        "neutron": {
            "security_group_rule": 100,
            "subnet": 0,
            "network": 0,
            "floatingip": 0,
            "security_group": 40,
            "router": 0,
            "port": 0
        },
        "octavia": {
            'load_balancer': 0,
            'listener': 0,
            'member': 0,
            'pool': 0,
            'health_monitor': 0,
        },
    },
    "xsmall": {
        "cinder": {
            "gigabytes": 2500,
            "volumes": 10,
            "snapshots": 25
        },
        "nova": {
            "metadata_items": 128,
            "injected_file_content_bytes": 10240,
            "ram": 32768,
            "floating_ips": 5,
            "key_pairs": 50,
            "instances": 5,
            "server_groups": 5,
            "injected_files": 5,
            "cores": 10,
            "fixed_ips": 0,
        },
        "neutron": {
            "security_group_rule": 300,
            "subnet": 2,
            "network": 2,
            "floatingip": 5,
            "security_group": 40,
            "router": 2,
            "port": 25
        },
        "octavia": {
            'load_balancer': 2,
            'listener': -1,
            'member': 10,
            'pool': 10,
            'health_monitor': 10,
        },
    },
    "small": {
        "cinder": {
            "gigabytes": 5000,
            "volumes": 20,
            "snapshots": 50
        },
        "nova": {
            "metadata_items": 128,
            "injected_file_content_bytes": 10240,
            "ram": 65536,
            "floating_ips": 10,
            "key_pairs": 50,
            "instances": 10,
            "server_groups": 10,
            "injected_files": 5,
            "cores": 20,
            "fixed_ips": 0,
        },
        "neutron": {
            "security_group_rule": 300,
            "subnet": 3,
            "network": 3,
            "floatingip": 10,
            "security_group": 40,
            "router": 3,
            "port": 50
        },
        "octavia": {
            'load_balancer': 5,
            'listener': -1,
            'member': 20,
            'pool': 10,
            'health_monitor': 10,
        },
    },
    "medium": {
        "cinder": {
            "gigabytes": 10000,
            "volumes": 100,
            "snapshots": 300
        },
        "nova": {
            "metadata_items": 128,
            "injected_file_content_bytes": 10240,
            "ram": 327680,
            "floating_ips": 25,
            "key_pairs": 50,
            "instances": 50,
            "server_groups": 50,
            "injected_files": 5,
            "cores": 100,
            "fixed_ips": 0,
        },
        "neutron": {
            "security_group_rule": 600,
            "subnet": 5,
            "network": 5,
            "floatingip": 25,
            "security_group": 70,
            "router": 5,
            "port": 250
        },
        "octavia": {
            'load_balancer': 15,
            'listener': -1,
            'member': 60,
            'pool': 30,
            'health_monitor': 30,
        },
    },
    "large": {
        "cinder": {
            "gigabytes": 50000,
            "volumes": 200,
            "snapshots": 600
        },
        "nova": {
            "metadata_items": 128,
            "injected_file_content_bytes": 10240,
            "ram": 655360,
            "floating_ips": 50,
            "key_pairs": 50,
            "instances": 100,
            "server_groups": 100,
            "injected_files": 5,
            "cores": 200,
            "fixed_ips": 0,
        },
        "neutron": {
            "security_group_rule": 900,
            "subnet": 10,
            "network": 10,
            "floatingip": 50,
            "security_group": 100,
            "router": 10,
            "port": 500
        },
        "octavia": {
            'load_balancer': 30,
            'listener': -1,
            'member': 120,
            'pool': 60,
            'health_monitor': 60,
        },
    },
    "xlarge": {
        "cinder": {
            "gigabytes": 100000,
            "volumes": 400,
            "snapshots": 1200
        },
        "nova": {
            "metadata_items": 256,
            "injected_file_content_bytes": 20480,
            "ram": 1310720,
            "floating_ips": 100,
            "key_pairs": 100,
            "instances": 200,
            "server_groups": 200,
            "injected_files": 10,
            "cores": 400,
            "fixed_ips": 0,
        },
        "neutron": {
            "security_group_rule": 1600,
            "subnet": 20,
            "network": 20,
            "floatingip": 100,
            "security_group": 200,
            "router": 20,
            "port": 1000
        },
        "octavia": {
            'load_balancer': 60,
            'listener': -1,
            'member': 240,
            'pool': 120,
            'health_monitor': 120,
        },
    }
}

sizes_ascending = [
    i[0] for i in sorted(sizes.items(), key=lambda k_v: k_v[1]['nova']['cores'])]
