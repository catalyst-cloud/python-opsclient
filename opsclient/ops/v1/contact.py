# Copyright (c) 2016 Catalyst IT Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import defaultdict
import sys
import traceback

from osc_lib.command import command
from six.moves.configparser import ConfigParser

from opsclient.common.odoo.client import OdooClient
from opsclient.common import output
from opsclient.common import profile


CONTACT_TYPES = ['billing', 'legal', 'owner', 'primary', 'technical']


class OdooContactList(command.Lister):
    """List contacts in Odoo that map back to a project in OpenStack.

    If the --project-id or --hypervisor options are not specified, then it
    will read project_id(s) from STDIN. AN example:

    cat <tenant-id-file> |  openstack catalyst contact list  \
        --odoo-ini odoo.ini \
        --contact-types primary,technical

    NOTE: Catalyst is a special and slighly broken case with MANY primary
    contacts. So, if project belongs to Catalyst, will only print partner
    information associated with the project.
    """

    def get_parser(self, prog_name):
        contact_args = super(OdooContactList, self).get_parser(prog_name)
        contact_args.add_argument(
            '--project-id',
            nargs='*', default=[],
            help='List of project ids to find contacts for in OpenStack.')
        contact_args.add_argument(
            '--hypervisor',
            required=False,
            help='If provided with the hostname of a hypervisor'
            ' the script will find the contacts for all compute'
            ' instances running on it. Please use the host name'
            ' only (eg: cat-por-comp001), not the FQDN.')
        contact_args.add_argument(
            '--contact-types',
            default='primary', required=False,
            help='The contact types will be searched, separated by comma, '
                 'please choose from "all,billing,legal,owner,primary,'
                 'technical". The default is primary (the person that signed '
                 'up for the cloud).')
        contact_args.add_argument(
            '--odoo-ini',
            default='odoo.ini',
            help='odoo.ini file to use. Defaults to "odoo.ini"')
        return contact_args

    def _projects_on_host(self, hypervisor):
        nova = self.app.client_manager.compute
        servers = nova.servers.list(search_opts={'all_tenants': True,
                                                 'host': hypervisor})
        project_list = set([str(s.tenant_id) for s in servers])
        return project_list

    def _get_contacts(self, parsed_args):
        """Get contacts for projects.
        """

        # Get projects from odoo
        search = [('tenant_id', 'in', list(self.projects)), ]
        odoo_projects = self.odooclient.projects.list(search, read=True)

        odoo_project_ids = [p['id'] for p in odoo_projects]

        # A mapping of odoo project ids to openstack project data.
        project_odoo_map = {
            p['id']: {
                'id': p['tenant_id'],
                'name': p['name'],
            }
            for p in odoo_projects
        }

        contact_types = set(parsed_args.contact_types.split(','))
        if 'all' in contact_types:
            contact_types = CONTACT_TYPES

        # Now we get all the relationships of the right types
        search = [
            ('cloud_tenant', 'in', odoo_project_ids),
            ('contact_type', 'in', list(contact_types)),
        ]
        project_rels = self.odooclient.project_relationships.list(
            search, read=True)

        # This gives us all the partner ids for the relationships
        partner_ids = set([p['partner_id'][0] for p in project_rels])

        # a mapping of partners to their project_relationships
        partner_project_map = defaultdict(list)
        for project_rel in project_rels:
            partner_project_map[project_rel['partner_id'][0]].append({
                'type': project_rel['contact_type'],
                'project': project_rel['cloud_tenant'][0],
            })

        # Now we get all the partner data
        search = [('id', 'in', list(partner_ids)), ]
        partners = self.odooclient.partners.list(search, read=True)

        projects_found = []
        # We want to create a contact line for every relationship:
        for partner in partners:
            for rel in partner_project_map[partner['id']]:
                project = project_odoo_map[rel['project']]
                projects_found.append(project['id'])
                contact_row = [
                    project['id'],
                    project['name'],
                    rel['type']
                ]
                for field in self.fields:
                    contact_row.append(partner[field])

                self.contacts.append(contact_row)

        # Just in case some projects weren't setup in odoo, we list that too.
        missing_project = list(set(projects_found) - set(self.projects))
        for project_id in missing_project:
            project_name = self.keystone.projects.get(project_id).name

            contact_row += [
                project_id,
                project_name,
                'None'
            ]
            for field in self.fields:
                contact_row.append('None')

            self.contacts.append(contact_row)

        # Now sort them by project_name
        self.contacts.sort(key=lambda contact: contact[1])

    def take_action(self, parsed_args):
        headers = [
            'Project id', 'Project name', 'contact_type', 'name',
            'email', 'phone', 'mobile']
        self.fields = ['name', 'email', 'phone', 'mobile']

        self.keystone = self.app.client_manager.identity

        self.projects = []
        if parsed_args.project_id:
            self.projects = parsed_args.project_id
        if parsed_args.hypervisor:
            self.projects.extend(
                self._projects_on_host(parsed_args.hypervisor)
            )
        if not sys.stdin.isatty():
            print("Reading project_ids from stdin.")
            for project_id in sys.stdin:
                self.projects.append(project_id.strip())

        if not self.projects:
            output.print_yellow(
                'No project ids supplied, '
                'getting contacts for all enabled projects.')
            self.projects = [
                p.id for p in self.keystone.projects.list()
                if p.enabled
            ]

        # Validate contact types
        contact_types = parsed_args.contact_types.split(',')
        for type in contact_types:
            if type != 'all' and type not in CONTACT_TYPES:
                output.print_red('ERROR: Wrong contact type specified.')
                return (headers, [])

        conf = ConfigParser()
        conf.read([parsed_args.odoo_ini])
        self.odooclient = OdooClient(conf._sections)

        self.contacts = []

        try:
            with profile.timed("Total Odoo time"):
                self._get_contacts(parsed_args)
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(
                exc_type, exc_value, exc_traceback,
                file=sys.stdout)
        except KeyboardInterrupt:
            print("...Terminating.")

        return (headers, self.contacts)
