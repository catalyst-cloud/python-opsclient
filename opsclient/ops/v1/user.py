# Copyright (c) 2018 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from datetime import datetime

from keystoneauth1.exceptions.http import NotFound

from osc_lib.command import command


class TerminateUser(command.Command):
    """
    For the given user, clear all roles across all projects and domains,
    remove user from all groups, then disable the user.
    """

    def get_parser(self, prog_name):
        parser = super(TerminateUser, self).get_parser(prog_name)

        parser.add_argument(
            'user', metavar='<user>',
            help="User name or ID")

        parser.add_argument(
            '--no-disable', action='store_true',
            help="Don't disable the user",
            default=False
        )

        parser.add_argument(
            '--keep-groups', action='store_true',
            help="Don't remove user groups",
            default=False
        )

        parser.add_argument(
            '--op', action='store_true',
            help="Perform operations. The default case for safety is no-op.",
            default=False
        )

        return parser

    def take_action(self, parsed_args):
        ks = self.app.client_manager.identity

        projects = ks.projects.list() + ks.projects.list(is_domain=True)
        project_dict = {project.id: project for project in projects}

        roles = ks.roles.list()
        role_dict = {role.id: role.name for role in roles}

        try:
            user = ks.users.get(parsed_args.user)
        except NotFound:
            try:
                user = ks.users.list(name=parsed_args.user)[0]
            except IndexError:
                raise NotFound("User '%s' not found." % parsed_args.user)

        for assignment in ks.role_assignments.list(user=user):
            try:
                project = project_dict[assignment.scope['project']['id']]
                domain = None
                scope_str = "project: %s (%s)" % (project.name, project.id)
            except KeyError:
                # Skip system scope for now
                if 'system' in assignment.scope:
                    continue
                domain = project_dict[assignment.scope['domain']['id']]
                project = None
                scope_str = "domain: %s (%s)" % (domain.name, domain.id)

            if 'OS-INHERIT:inherited_to' in assignment.scope:
                inherit = True
            else:
                inherit = False

            if parsed_args.op:
                print("Removing%s role '%s' on %s" %
                      (' inherited' if inherit else '',
                       role_dict[assignment.role['id']], scope_str))
                try:
                    ks.roles.revoke(
                        assignment.role['id'],
                        user=user, project=project, domain=domain,
                        os_inherit_extension_inherited=inherit)
                except NotFound:
                    # Role already removed, do nothing.
                    pass
            else:
                print("Would remove%s role '%s' on %s" %
                      (' inherited' if inherit else '',
                       role_dict[assignment.role['id']], scope_str))

        if not parsed_args.keep_groups:
            for group in ks.groups.list(user=user):
                if parsed_args.op:
                    print("Removing user '%s' (%s) from group %s (%s)" %
                          (user.name, user.id, group.name, group.id))
                    ks.users.remove_from_group(user, group)
                else:
                    print("Would remove user '%s' (%s) from group %s (%s)" %
                          (user.name, user.id, group.name, group.id))

        disabled_by = ks.users.get(
            user=self.app.client_manager.auth_ref.user_id)
        disabled_by = (
            "%s@%s" % (disabled_by.name, disabled_by.domain_id))

        if not parsed_args.no_disable and parsed_args.op:
            print("Disabling user: %s (%s)" % (user.name, user.id))
            ks.users.update(
                user, enabled=False,
                disabled_at=datetime.utcnow(),
                disabled_by=disabled_by)
        elif not parsed_args.no_disable:
            print("Would disable user: %s (%s)" % (user.name, user.id))
