# Copyright (c) 2016 Catalyst IT Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple
import datetime
import logging
from osc_lib.command import command

Image = namedtuple('Image', ['id', 'name', 'size', 'min_disk', 'min_ram'])
PING_TIMEOUT = 300
IMAGE_MAPS = {'ubuntu-16.04-x86_64': {'url': 'https://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img', 'user': 'ubuntu'},    # noqa
              'ubuntu-14.04-x86_64': {'url': 'https://cloud-images.ubuntu.com/trusty/current/trusty-server-cloudimg-amd64-disk1.img', 'user': 'ubuntu'},    # noqa
              'ubuntu-12.04-x86_64': {'url': 'https://cloud-images.ubuntu.com/precise/current/precise-server-cloudimg-amd64-disk1.img', 'user': 'ubuntu'},    # noqa 
              'debian-8-x86_64':  {'url': 'http://cdimage.debian.org/cdimage/openstack/current/debian-8.4.0-openstack-amd64.raw', 'description': ''},    #noqa
              'centos-6.6-x86_64': {'url': 'http://cloud.centos.org/centos/6.6/images/CentOS-6-x86_64-GenericCloud.qcow2', 'user': 'centos'},    # noqa
              'centos-7.0-x86_64': {'url': 'http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2', 'user': 'centos'},    # noqa
              'coreos': {'url': 'http://stable.release.core-os.net/amd64-usr/current/coreos_production_openstack_image.img.bz2', 'version': 'http://stable.release.core-os.net/amd64-usr/current/version.txt'},    # noqa
              'atomic-7-x86_64': {'url': 'http://cloud.centos.org/centos/7/atomic/images/CentOS-Atomic-Host-7-GenericCloud.qcow2'}
              }
# Network Name, ID and flavor ID are hardcoded since they're not changed
REGIONS = {'nz-por-1': {'network_id': '9e06c022-c1e6-4c0c-a09d-ea66e0129a84',
                        'flavor_id': '1750075c-cd8a-4c87-bd06-a907db83fec6'},
           'nz_wlg_2': {'network_id': '5f9fca20-eb64-4771-bfdf-10f9deafd2a7',
                        'flavor_id': '2ba44887-5fe3-44cc-8bca-e2b60a206a66'}}
KEYPAIR_NAME = 'auto_image_updater'
NETWORK_FOR_SSH = 'network-4-image-update'


class ImageUpdate(command.Command):
    """Automatically update Catalyst Cloud public images."""

    log = logging.getLogger(__name__ + ".ImageUpdate")

    def get_parser(self, prog_name):
        parser = super(ImageUpdate, self).get_parser(prog_name)
        parser.add_argument("--download-path", metavar="<download-path>",
                            default='/tmp', dest="download_path",
                            help="Path for storing images data.")
        parser.add_argument("--update-all-regions", type=bool,
                            metavar="update-all-regions", default=True,
                            dest="update_all_regions",
                            help="Indicate if update all regions or current "
                            "default region.")
        parser.add_argument('--auto-publish', type=bool, default=True,
                            metavar='<auto-publish>', dest='auto_publish',
                            help='Indicate if publish the new images '
                            'automatically.')
        return parser

    def take_action(self, parsed_args):
        update(self.app.client_manager, parsed_args)


def update(client, parsed_args):
    """Update images automatically."""
    regions = (REGIONS.keys() if parsed_args.update_all_regions
               else [client.region_name])
    for region in regions:
        print('*' * 79)
        print('Start to update images on region: %s' % region)
        # Re-initialize the clients based on the region changing
        client.region_name =region

        client.image_path = parsed_args.download_path
        client.flavor_id = REGIONS[region]['flavor_id']
        client.network_id = REGIONS[region]['network_id']
        client.network_for_ssh = NETWORK_FOR_SSH

        project_id = client.identity.session.auth.auth_ref.project_id
        images = client.image.images.list(owner=project_id)
        keypair = _create_keypair(client)
        today = datetime.datetime.today()
        try:
            # Iterate all the images which in public status
            for image in images:
                try:
                    struct_time = time.mktime(time.strptime(image.created_at,
                                                        '%Y-%m-%dT%H:%M:%S'))
                except ValueError:
                    struct_time = time.mktime(time.strptime(image.created_at,
                                                        '%Y-%m-%dT%H:%M:%S.000000'))
                created_at = datetime.datetime.fromtimestamp(struct_time)

                if re.match(r'.*-\d{8}', image.name):
                    continue

                if image.is_public is False or (image.name not in IMAGE_MAPS and
                                                'coreos' not in image.name):
                    continue

                if ('coreos' in image.name and
                        'coreos' not in [x[:6] for x in IMAGE_MAPS]):
                    continue

                # NOTE(flwang): If the image has been updated in 30 days, do
                # nothing.
                if today - created_at < datetime.timedelta(days=30):
                    continue

                print('Start to update image: %s' % image.name)
                _update_image(client, parsed_args, image, keypair)
        except Exception as e:
            raise e
        finally:
            client.compute.keypairs.delete('auto_image_updater')


def _update_image(client, parsed_args, image, keypair):
    # 1. Download latest image and verify checksum
    new_image_path, new_image_name = _download_image(client, image)

    if not new_image_path:
        print('Failed to download image for %s' % image.name)
        return

    # 2. Create instance with the image
    new_image = _create_image(client, image, new_image_path, new_image_name)

    # 3. Verify image
    pingable = _verify_image(client, new_image, keypair)

    if pingable:
        if parsed_args.auto_publish:
            # 4. Publish the new image
            _publish_image(client, new_image)

            if image.id:
                # If image.id is None, then means we're adding a new image so
                # don't have to unpublish the old one.
                # 5. Un-publish the old image
                _unpublish_image(client, image)
            print('#' * 39)
        else:
            print('The new image has been created successfully but you need '
                  ' publish it and unpublish the old one manually.')
    else:
        print('Failed to update image:{0},{1}'.format(image.name,
                                                      image.id))


def _download_image(client, image):
    print('Start to download image data: %s' % image.name)

    def convert_image(source, dest, out_format):
        cmd = ('qemu-img', 'convert', '-O', out_format, source, dest)
        proc = subprocess.Popen(cmd,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        proc.communicate()
        return (proc.returncode == 0)

    img_key = image.name if 'coreos' not in image.name else 'coreos'
    new_image_name = image.name

    if 'coreos' in image.name:
        resp = urllib.urlopen(IMAGE_MAPS[img_key]['version'])
        version = resp.read().split('\n')[3].split('=')[1]
        print('The new version of CoreOS is %s' % version)
        new_image_name = 'coreos-%s-x86_64' % version
        if image.name == new_image_name:
            return None, None

    data_url = IMAGE_MAPS[img_key]['url']
    file_name = os.path.basename(data_url)

    raw_file_path = os.path.join(client.image_path,
                                 file_name.replace('.img.bz2', '.raw'). \
                                 replace('.img', '.raw'). \
                                 replace('.qcow2', '.raw'))

    if os.path.exists(raw_file_path) :
        file_life = (datetime.datetime.now() - datetime.datetime.fromtimestamp(os.path.getctime(raw_file_path))) # noqa
        if file_life.days < 1:
            print('Reuse the existing file %s.' % raw_file_path)
            return raw_file_path, new_image_name
        else:
            os.remove(raw_file_path)

    if 'centos-6-x86_64' in file_name.lower():
        if '/6.5/' in data_url:
            file_name = file_name.replace('-6-', '-6.5-')
        elif '/6.6/' in data_url:
            file_name = file_name.replace('-6-', '-6.6-')

    if (os.path.exists(file_name) and 'coreos' in image.name):
        pass
    else:
        file_path = os.path.join(client.image_path, file_name)
        try:
            urllib.urlretrieve(data_url, file_path)
        except Exception as e:
            raise e
    if file_name.endswith('.img.bz2'):
        zipfile = bz2.BZ2File(file_path)
        data = zipfile.read()
        file_path = file_path.replace('.img.bz2', '.img')
        open(file_path, 'wb').write(data)
        os.remove(file_path + '.bz2')

    source_file_path = file_path
    dest_file_path = file_path.replace('.img', '.raw').replace('.qcow2', '.raw')

    # TODO(flwang): Check the image format is default with qemu-img
    # instead of converting it directly
    if file_path.endswith('.img') or file_path.endswith('.qcow2'):
        convert_image(source_file_path, dest_file_path, 'raw')
        # Clean up
        os.remove(file_path)

    # Be consistent
    file_path = dest_file_path

    print('Download image data completely for %s' % image.name)

    return file_path, new_image_name


def _create_image(client, image, new_image_path, new_image_name):
    # If the image size is more than 8G, just use it as the size
    image_size = image.size if image.size > 8589934592 else 8589934592
    new_image = client.image.images.create(name=new_image_name,
                                           container_format='bare',
                                           disk_format='raw',
                                           min_disk=image.min_disk,
                                           min_ram=image.min_ram,
                                           size=image_size,
                                           data=open(new_image_path, 'r'))
    print('Created new image in glance for %s' % image.name)
    return new_image


def _verify_image(client, new_image, keypair):
    instance = _boot_instance_from_image(client, new_image, keypair)

    pingable = False

    if instance and instance.addresses:
        ip_address = None
        for addr in instance.addresses[client.network_for_ssh]:
            if addr['OS-EXT-IPS:type'] == 'floating':
                ip_address = addr['addr']
        if ip_address:
            pingable = ping_ip_address(ip_address)

    _delete_server(client, instance)

    print('The instance created by new image is pingable? %s' % pingable)

    return pingable


def _publish_image(client, new_image):
    client.image.images.update(new_image, is_public=True, protected=True)
    print('Public the new image successfully.')


def _unpublish_image(client, old_image):
    # NOTE(flwang): Don't update image as private since it may cause nova
    # rebuild failure, so now we just rename them and hide them on horizon.
    today = datetime.datetime.today().strftime('%Y%m%d')
    client.image.images.update(old_image,
                               name=old_image.name + '-' + today)

    print('The old image is renamed as %s' % old_image.name + '-' + today)


def _create_keypair(client):
    try:
        client.compute.keypairs.delete(KEYPAIR_NAME)
    except Exception:
        pass
    keypair = client.compute.keypairs.create(KEYPAIR_NAME)
    return keypair


def _boot_instance_from_image(client, image, keypair):
    create_kwargs = {
        'key_name': keypair.name
    }
    instance_name = image.name + '_instance'
    return _create_server(client, name=instance_name, image=image.id,
                          create_kwargs=create_kwargs)


def _create_server(client, name, image, flavor=None,
                   wait_on_boot=True, create_kwargs=None):
    if flavor is None:
        flavor = client.flavor_id

    floating_ip = client.compute.floating_ips.create()
    time.sleep(10)
    server = client.compute.servers.create(name, image, flavor,
                                       nics=[{'net-id': client.network_id}],
                                       **create_kwargs)
    time.sleep(20)
    server.add_floating_ip(floating_ip)
    # Sometimes, nova/neutron needs some time to wait for the floating ip
    # to take effect
    time.sleep(10)

    return server


def _delete_server(client, instance):
    client.compute.servers.delete(instance.id)


def ping_ip_address(ip_address, ping_timeout=None):
    timeout = ping_timeout or PING_TIMEOUT
    cmd = ['ping', '-c1', '-w1', ip_address]

    def ping():
        proc = subprocess.Popen(cmd,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        proc.communicate()
        return (proc.returncode == 0)

    return call_until_true(ping, timeout, 1)


def call_until_true(func, duration, sleep_for):
    now = time.time()
    timeout = now + duration
    while now < timeout:
        if func():
            return True
        time.sleep(sleep_for)
        now = time.time()
    return False
