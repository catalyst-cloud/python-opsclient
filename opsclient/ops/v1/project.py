# Copyright (c) 2016 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from collections import defaultdict
import datetime
import six
import sys
import time

from cinderclient import client as cinderclient
from glanceclient import client as glanceclient
from keystoneauth1 import session as ksession
from keystoneauth1.identity import generic
from keystoneauth1.exceptions import EndpointNotFound
from keystoneclient.exceptions import NotFound
from neutronclient.common import exceptions as neutron_exceptions
from neutronclient.v2_0 import client as neutronclient
from magnumclient import client as magnumclient
from novaclient import client as novaclient
from openstack.exceptions import SDKException
from openstack import connection


from osc_lib.command import command

from opsclient.common import prompt
from opsclient.common import output


class ProjectTerminate(command.Command):
    """Terminate project and all its resources.

    Will disable project and delete all resources associated with it to
    avoid leaving any legacy stuff.
    """

    def get_parser(self, prog_name):
        parser = super(ProjectTerminate, self).get_parser(prog_name)
        parser.add_argument(
            "project_id", metavar='<project_id>',
            help="Project ID to terminate.")
        parser.add_argument(
            '--auto-clean', action='store_true',
            help='WARNING: Auto clean the resources without prompt.')
        parser.add_argument(
            '--reenable', action='store_true',
            help='If disabled, this will reenable the project to clear it.')
        return parser

    def take_action(self, parsed_args):
        identity = self.app.client_manager.identity

        self.warn_and_confirm(parsed_args, identity)

        # First lets ensure we don't have admin on the project:
        try:
            identity.roles.revoke(
                identity.roles.find(name='admin'),
                user=self.app.client_manager.auth_ref.user_id,
                project=parsed_args.project_id)
        except NotFound:
            pass

        # Lets grant ourselves roles in the project.
        identity.roles.grant(
            identity.roles.find(name='_member_'),
            user=self.app.client_manager.auth_ref.user_id,
            project=parsed_args.project_id)
        try:
            identity.roles.grant(
                identity.roles.find(name='heat_stack_owner'),
                user=self.app.client_manager.auth_ref.user_id,
                project=parsed_args.project_id)
        except NotFound:
            pass

        regions = [region.id for region in identity.regions.list()]

        print("#### Starting disable process. ####")

        # To make things easier, we will scope ourselves
        # to the project we want to clear.
        kwargs = {
            'token': self.app.client_manager.auth_ref.auth_token,
            'auth_url': self.app.client_manager.session.auth.auth_url,
            'project_id': parsed_args.project_id,
            'project_domain_id':
                self.app.client_manager.auth_ref.project_domain_id,
        }
        auth = generic.Token(**kwargs)
        session = ksession.Session(auth=auth, verify=True)

        for region in regions:
            print("#### STARTING REGION: %s ####" % region)

            clients = {
                "compute": novaclient.Client(
                    2, session=session, region_name=region),
                "network": neutronclient.Client(
                    session=session, region_name=region),
                "volume": cinderclient.Client(
                    2, session=session, region_name=region),
                "image": glanceclient.Client(
                    2, session=session, region_name=region),
                "container_infrastructure_management": magnumclient.Client(
                    '1', session=session, region_name=region)
            }

            conn = connection.Connection(
                session=session,
                region=region,
                verify=session.verify,
                cert=session.cert)

            clients['object-store'] = conn.object_store
            clients['orchestration'] = conn.orchestration
            clients['load_balancer'] = conn.load_balancer

            output.print_yellow('#### PURGING MAGNUM ####')
            self.magnum_delete(parsed_args, clients)

            output.print_yellow('#### PURGING HEAT ####')
            self.heat_delete(parsed_args, clients)

            output.print_yellow('#### PURGING NOVA ####')
            self.nova_delete(parsed_args, clients)

            output.print_yellow('#### PURGING OCTAVIA ####')
            self.octavia_delete(parsed_args, clients)

            output.print_yellow('#### PURGING NEUTRON ####')
            self.neutron_delete(parsed_args, clients)

            output.print_yellow('#### PURGING CINDER ####')
            self.cinder_delete(parsed_args, clients)

            output.print_yellow('#### PURGING GLANCE ####')
            self.glance_delete(parsed_args, clients)

            output.print_yellow('#### PURGING SWIFT ####')
            self.swift_delete(parsed_args, clients)

        identity.roles.revoke(
            identity.roles.find(name='_member_'),
            user=self.app.client_manager.auth_ref.user_id,
            project=parsed_args.project_id)
        try:
            identity.roles.revoke(
                identity.roles.find(name='heat_stack_owner'),
                user=self.app.client_manager.auth_ref.user_id,
                project=parsed_args.project_id)
        except NotFound:
            pass

        output.print_yellow('#### PURGING KEYSTONE ####')
        self.keystone_disable(parsed_args, identity)

    def warn_and_confirm(self, parsed_args, identity_client):
        project = identity_client.projects.get(parsed_args.project_id)

        if not parsed_args.reenable and not project.enabled:
            output.print_yellow("Project is already disabled, can't proceed.")
            output.print_yellow(
                "Add the '--reenable' option to your command if you wish to "
                "remove resources from a disabled project.")
            sys.exit()

        output.print_red(output.SKULL)
        output.print_yellow(
            "WARNING: EVERYTHING owned by the following project "
            "(on all regions) will be deleted and the project will "
            "be disabled.")

        output.print_dict(project.to_dict())

        assignments = identity_client.role_assignments.list(
            project=parsed_args.project_id)

        print("The users and their roles in project '%s' is as below:" %
              project.name)

        roles = identity_client.roles.list()
        role_dict = {role.id: role.name for role in roles}

        user_roles = defaultdict(list)
        for assignment in assignments:
            user_roles[assignment.user['id']].append(
                role_dict[assignment.role['id']])

        user_list = []
        for user_id, roles in six.iteritems(user_roles):
            user_list.append([
                identity_client.users.get(user_id).name,
                user_id,
                roles
            ])
        output.print_list_rows(
            user_list, ['name', 'id', 'roles'])

        if not prompt.prompt_safe(
                "Project '%s' will be disabled and all resources deleted." %
                project.name):
            sys.stdout.write("Number you typed was incorrect. Exiting...\n")
            sys.exit()
        if parsed_args.auto_clean and not prompt.prompt_safe(
                "Auto-clean is on, are you REALLY sure? "
                "There is no going back."):
            sys.stdout.write("Number you typed was incorrect. Exiting...\n")
            sys.exit()

        if not project.enabled and parsed_args.reenable:
            print("Reenabling the project.")
            identity_client.projects.update(
                parsed_args.project_id, enabled=True)

    def keystone_disable(self, parsed_args, identity_client):
        try:
            if(parsed_args.auto_clean or
                    prompt.prompt_yes_no(
                        'Do you want to clear and disable '
                        'the project in Keystone?: ')):

                disabled_by = identity_client.users.get(
                    user=self.app.client_manager.auth_ref.user_id)
                disabled_by = (
                    "%s@%s" % (disabled_by.name, disabled_by.domain_id))

                # Get all users on the project
                assignments = identity_client.role_assignments.list(
                    project=parsed_args.project_id)
                user_ids = set()
                for assignment in assignments:
                    if hasattr(assignment, 'user'):
                        user_ids.add(assignment.user['id'])

                for user_id in user_ids:
                    assignments = identity_client.role_assignments.list(
                        user=user_id)

                    user_roles = defaultdict(list)
                    for assignment in assignments:
                        try:
                            scope_id = assignment.scope['project']['id']
                        except KeyError:
                            # we don't care about non-project scopes
                            continue
                        user_roles[scope_id].append(assignment.role['id'])

                    # We clear ALL roles from this project for all users.
                    for role in user_roles[parsed_args.project_id]:
                        identity_client.roles.revoke(
                            role, user=user_id,
                            project=parsed_args.project_id)

                    # if user has roles only on this projects then disable user
                    user_projects = list(user_roles.keys())
                    if (len(user_projects) == 1 and
                            user_projects[0] == parsed_args.project_id):
                        identity_client.users.update(
                            user=user_id, enabled=False,
                            disabled_at=datetime.datetime.utcnow(),
                            disabled_by=disabled_by)

                # Disable project
                identity_client.projects.update(
                    parsed_args.project_id, enabled=False,
                    disabled_at=datetime.datetime.utcnow(),
                    disabled_by=disabled_by)

                # TODO(flwang):Disable customer in odoo, which is not supported
                # in Odoo
                output.print_dict(
                    identity_client.projects.get(
                        parsed_args.project_id).to_dict())

                print(
                    "\nProject: '%s' has been cleared and disabled." %
                    parsed_args.project_id)
            else:
                print("\nProject not disabled in Keystone.")
        except Exception as e:
            output.print_red("Error during keystone disable: %s" % e)
            raise

    def _ensure_correct_project(self, owner, resources):
        """because it pays to be paranoid"""
        safe_list = []
        for res in resources:
            res_owner = (
                getattr(res, "project_id", None) or
                getattr(res, "tenant_id", None) or
                getattr(res, "owner", None) or
                getattr(res, "user_project_id", None) or
                getattr(res, "os-vol-tenant-attr:tenant_id", None) or
                getattr(res, "os-extended-snapshot-attributes:project_id",
                        None)
            )
            if res_owner == owner:
                safe_list.append(res)
        return safe_list

    def nova_delete(self, parsed_args, clients):
        print('#### SERVER (VM) LIST ####')
        servers = clients['compute'].servers.list()
        servers = self._ensure_correct_project(
            parsed_args.project_id, servers)
        output.print_list(servers, ['id', 'name', 'status', 'tenant_id'])

        if (len(servers) and (
                parsed_args.auto_clean or
                prompt.prompt_yes_no('Delete Servers?: '))):
            for server in servers:
                clients['compute'].servers.delete(server.id)

            print("Servers terminating, waiting for confirmation...")
            while True:
                time.sleep(10)
                servers = clients['compute'].servers.list()
                servers = self._ensure_correct_project(
                    parsed_args.project_id, servers)

                if not servers:
                    print("All servers terminated.")
                    break
                print('#### SERVERS STILL ACTIVE: ####')
                output.print_list(
                    servers, ['id', 'name', 'status', 'tenant_id'])
                print("Servers still terminating, waiting for confirmation...")
        else:
            print("Not deleting servers in Nova.")

    def glance_delete(self, parsed_args, clients):
        print('#### IMAGE LIST ####')
        # '.images.list()' returns a generator, so we make it a list:
        images = list(clients['image'].images.list())
        images = self._ensure_correct_project(
            parsed_args.project_id, images)
        output.print_list(images, ['id', 'name', 'owner'])

        if (len(images) and (
                parsed_args.auto_clean or
                prompt.prompt_yes_no('Delete Images?: '))):
            for image in images:
                clients['image'].images.delete(image.id)
        else:
            print("Not deleting images in Glance.")

    def cinder_delete(self, parsed_args, clients):
        print('#### VOLUME SNAPSHOT LIST ####')
        snapshots = clients['volume'].volume_snapshots.list()
        snapshots = self._ensure_correct_project(
            parsed_args.project_id, snapshots)
        output.print_list(snapshots, ['id', 'name', 'status'])

        if (len(snapshots) and (
                parsed_args.auto_clean or
                prompt.prompt_yes_no('Delete Volume Snapshots?: '))):
            for snapshot in snapshots:
                clients['volume'].volume_snapshots.delete(snapshot.id)
            # Since we can't delete volumes with snapshots, we have to wait
            # and check they are all deleted.
            print("Volume Snapshots terminating, waiting for confirmation...")
            while True:
                time.sleep(5)
                snapshots = clients['volume'].volume_snapshots.list()
                snapshots = self._ensure_correct_project(
                    parsed_args.project_id, snapshots)

                if not snapshots:
                    print("All Volume Snapshots terminated.")
                    break
                print('#### VOLUME SNAPSHOT STILL ACTIVE: ####')
                output.print_list(
                    snapshots, ['id', 'name', 'status'])
                print("Volume Snapshots still terminating, waiting...")

        else:
            print("Not deleting snapshots in Cinder.")

        print('#### VOLUME LIST ####')
        volumes = clients['volume'].volumes.list()
        volumes = self._ensure_correct_project(
            parsed_args.project_id, volumes)
        output.print_list(volumes, ['id', 'name', 'status'])

        if (len(volumes) and (
                parsed_args.auto_clean or
                prompt.prompt_yes_no('Delete Volumes?: '))):
            for volume in volumes:
                clients['volume'].volumes.delete(volume.id)
        else:
            print("Not deleting volumes in Cinder.")

    def swift_delete(self, parsed_args, clients):
        print('#### CONTAINER LIST ####')
        try:
            containers = list(clients['object-store'].containers())
        except SDKException as e:
            if e.message == ("Could not find requested "
                             "endpoint in Service Catalog."):
                print("Swift not setup.")
                return
            else:
                raise
        output.print_list(containers, ['name', 'count', 'bytes'])

        if (len(containers) and (
                parsed_args.auto_clean or
                prompt.prompt_yes_no('Delete Swift resources?: '))):
            for container in containers:
                for obj in clients['object-store'].objects(container):
                    clients['object-store'].delete_object(obj)
                clients['object-store'].delete_container(container)
        else:
            print("Not deleting containers and objects in Swift.")

    def neutron_delete(self, parsed_args, clients):
        client = clients['network']

        # vpn service
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'ipsec_site_connection')
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'ipsecpolicy')
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'ikepolicy')
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'vpnservice')

        # security group
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'security_group')

        # ports
        print('#### PORT LIST ####')
        try:
            ports = client.list_ports()['ports']
            delete_ports = []
            for port in ports:
                if port['tenant_id'] == parsed_args.project_id:
                    delete_ports.append(port)
            output.print_list(delete_ports, ['id', 'name', 'tenant_id'])

            if (len(delete_ports) and (
                    parsed_args.auto_clean or
                    prompt.prompt_yes_no('Delete Ports?:'))):
                for port in delete_ports:
                    try:
                        device_owner = port['device_owner']
                        if device_owner == 'network:router_gateway':
                            client.remove_gateway_router(port['device_id'])
                        elif device_owner == 'network:router_interface':
                            for subnet in port['fixed_ips']:
                                body = {'subnet_id': subnet['subnet_id']}
                                client.remove_interface_router(
                                    port['device_id'], body)
                        else:
                            client.delete_port(port['id'])
                    except Exception as e:
                        output.print_red(
                            'Failed to delete port: %s, see: %s' %
                            (port['id'], str(e)))
                        continue
        except neutron_exceptions.NotFound:
            print("Port resource not setup.")

        # floating IP
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'floatingip')

        # router
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'router')

        # network
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'network')

        # sub net
        self._delete_targeted_neutron_resource(
            client, parsed_args, 'subnet')

    def _delete_targeted_neutron_resource(self, client, parsed_args, resource):
        print('#### %s LIST ####' % resource.upper())
        list_command = 'list_%ss' % resource
        if resource in ('ipsecpolicy', 'ikepolicy'):
            list_command = list_command.replace('policy', 'policie')
        try:
            resources = getattr(client, list_command)()
        except neutron_exceptions.NotFound:
            print(
                "%s resource not setup." %
                resource.replace("_", " ").title())
            return

        res_key = resource + 's'
        if resource in ('ipsecpolicy', 'ikepolicy'):
            res_key = res_key.replace('policy', 'policie')
        delete_resources = []
        for res in resources[res_key]:
            if res['tenant_id'] == parsed_args.project_id:
                delete_resources.append(res)
        output.print_list(delete_resources, ['id', 'name', 'tenant_id'])

        if (len(delete_resources) and (
                parsed_args.auto_clean or
                prompt.prompt_yes_no(
                    'Delete %sS?: ' % resource.replace("_", " ").title()))):
            delete_function = getattr(
                client, 'delete_%s' % resource)
            for res in delete_resources:
                # special case for security_group
                if (resource == "security_group" and
                        res['name'] == "default"):
                    print("Can't delete default security group. Skipping.")
                    continue
                delete_function(res['id'])
        else:
            print("Not deleting %ss in Neutron." % resource)

    def heat_delete(self, parsed_args, clients):
        print('#### STACKS LIST ####')
        try:
            # .stacks() returns a generator, we want a list.
            stacks = list(clients['orchestration'].stacks())
        except EndpointNotFound:
            print("Heat not setup.")
            return
        stacks = self._ensure_correct_project(
            parsed_args.project_id, stacks)
        output.print_list(stacks, ['id', 'name', 'status'])

        if (len(stacks) and (
                parsed_args.auto_clean or
                prompt.prompt_yes_no('Delete Heat Stacks?: '))):
            for stack in stacks:
                clients['orchestration'].delete_stack(stack)
            print("Stacks terminating, waiting for confirmation...")
            while True:
                time.sleep(5)
                stacks = list(clients['orchestration'].stacks())
                stacks = self._ensure_correct_project(
                    parsed_args.project_id, stacks)

                if not stacks:
                    print("All Stacks terminated.")
                    break
                print('#### STACKS STILL ACTIVE: ####')
                output.print_list(
                    stacks, ['id', 'name', 'status'])
                print("Stacks still terminating, waiting...")
        else:
            print("Not deleting stacks in Heat.")

    def octavia_delete(self, parsed_args, clients):
        client = clients['load_balancer']

        print("### LOAD BALANCER LIST ###")

        load_balancers = list(client.load_balancers())
        load_balancers = self._ensure_correct_project(
            parsed_args.project_id, load_balancers)

        if load_balancers:
            output.print_list(load_balancers, ['id', 'name', 'project_id'])

            if parsed_args.auto_clean or prompt.prompt_yes_no(
                    'Delete load balancers?: '):

                for load_balancer in load_balancers:
                    client.delete_load_balancer(load_balancer, cascade=True)

                while True:
                    # Ensure we don't exit until all resources are deleted
                    time.sleep(10)
                    load_balancers = list(client.load_balancers())
                    load_balancers = self._ensure_correct_project(
                        parsed_args.project_id, load_balancers)

                    if not load_balancers:
                        break

                    print('#### LOAD BALANCERS STILL ACTIVE: ####')
                    output.print_list(
                        load_balancers, [
                            'id', 'name', 'provisioning_status',
                            'operating_status', 'project_id'])
                    print("Load balancers still terminating, "
                          "waiting for confirmation...")
        else:
            print("Not deleting load balancers in Octavia.")

    def magnum_delete(self, parsed_args, clients):
        client = clients['container_infrastructure_management']

        print("### MAGNUM CLUSTER LIST ###")

        clusters = client.clusters.list(detail=True)
        clusters = self._ensure_correct_project(
            parsed_args.project_id, clusters)

        if clusters:
            output.print_list(
                clusters,
                ['uuid', 'name', 'status', 'health_status', 'project_id'])

            if parsed_args.auto_clean or prompt.prompt_yes_no(
                    'Delete clusters?: '):

                for cluster in clusters:
                    client.clusters.delete(cluster.uuid)

                while True:
                    # Ensure we don't exit until all resources are deleted
                    time.sleep(10)
                    clusters = client.clusters.list(detail=True)
                    clusters = self._ensure_correct_project(
                        parsed_args.project_id, clusters)

                    if not clusters:
                        break

                    print('#### CLUSTERS STILL ACTIVE: ####')
                    output.print_list(
                        clusters,
                        ['uuid', 'name', 'status',
                         'health_status', 'project_id'])
                    print("Clusters still terminating, "
                          "waiting for confirmation...")
        else:
            print("Not deleting Clusters in Magnum.")
