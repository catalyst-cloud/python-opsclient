# Copyright (C) 2018 Catalyst IT Ltd
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


from collections import OrderedDict
import yaml

from keystoneclient import exceptions as ks_exceptions

from osc_lib.command import command

from opsclient.common import output
from opsclient.common.quota import QuotaManager
from opsclient.ops.sizes import sizes, sizes_ascending


class AuditQuota(command.Command):
    """
    Lists the current quota usage for a given project.
    """

    def get_parser(self, prog_name):
        quota_args = super(AuditQuota, self).get_parser(prog_name)

        quota_args.add_argument(
            'project', metavar='<project>',
            help='Show quota usage for this project.')

        quota_args.add_argument(
            '--regions', metavar='<regions>',
            help='Comma separated list of regions. Defaults to all regions.')

        quota_args.add_argument(
            '--services', metavar='<services>',
            help='Comma separated list of services. Defaults to: '
                 '"cinder,nova,neutron,octavia"',
            default='cinder,nova,neutron,octavia')

        return quota_args

    def take_action(self, parsed_args):
        regions = parsed_args.regions
        services = parsed_args.services
        project = parsed_args.project

        if not services:
            output.print_red("Must supply services or use defaults.")
            return
        else:
            services = services.split(",")

        keystone = self.app.client_manager.identity

        try:
            project_obj = keystone.projects.get(project)
            project_id = project_obj.id
        except ks_exceptions.NotFound:
            projects = keystone.projects.list(name=project)
            if projects:
                project_obj = projects[0]
            else:
                raise ks_exceptions.NotFound(
                    "Project '%s' not found." % project)
            project_id = project_obj.id

        print(
            "Getting quota usage for project: %s (%s)" % (
                project_obj.name, project_obj.id))

        if regions:
            regions = regions.split(",")
            active_services = {r: services for r in regions}
        else:
            regions = [region.id for region in keystone.regions.list()]
            active_services = {"*": services}

        quota_manager = QuotaManager(
            self.app.client_manager.session, project_id,
            active_services=active_services)

        for region in regions:

            output.print_yellow("Getting usage for region: %s" % region)

            region_data = quota_manager.get_region_quota_data(region)

            region_info = {
                'Region': region,
                'Quota Size': region_data['current_quota_size'],
                'Size options': region_data['quota_change_options'],
            }
            output.print_dict(region_info)

            for service, service_quota in region_data['current_usage'].items():
                quota_rows = []

                for key in service_quota.keys():
                    quota_rows.append([
                        key,
                        service_quota[key],
                        region_data['current_quota'][service][key]])

                for row in quota_rows:
                    if row[2] == 0:
                        if row[1] > 0:
                            percentage = "infinity %"
                        else:
                            percentage = "-- %"
                    elif row[2] < 0:
                        percentage = "-- %"
                    else:
                        percentage = "%.2f %%" % ((
                            float(row[1]) /
                            float(row[2])) * 100)
                    row.append(percentage)

                output.print_list_rows(quota_rows, [
                    '%s Quotas' % service.title(),
                    'Used', 'Limit', '(%) Used'])


class UpdateQuota(command.Command):
    """
    A simple script for updating a project quota to a given predefined
    size. Sizes come from "opsclient/ops/sizes.py".
    """

    def get_parser(self, prog_name):
        quota_args = super(UpdateQuota, self).get_parser(prog_name)

        quota_args.add_argument(
            'project',
            help='Change quota for this project.')

        quota_args.add_argument(
            '--size', metavar='<size>',
            choices=sizes.keys(),
            help='Select one of: %s' % ', '.join(sizes_ascending),
            required=True)

        quota_args.add_argument(
            '--regions', metavar='<regions>',
            help='Comma separated list of regions. Defaults to all regions.')

        quota_args.add_argument(
            '--services', metavar='<services>',
            help='Comma separated list of services. Defaults to: '
                 '"cinder,nova,neutron,octavia"',
            default='cinder,nova,neutron,octavia')

        quota_args.add_argument(
            '--no-op', action='store_true',
            help="Perform no operations.",
            default=False)

        return quota_args

    def find_differences(self, old, new):
        quota_differences = []
        for key in new.keys():
            new_val = new[key]
            old_val = old[key]
            if new_val > old_val:
                new_val = output.green(new_val)
            elif new_val < old_val:
                new_val = output.red(new_val)
            quota_differences.append([key, old_val, new_val])
        return quota_differences

    def take_action(self, parsed_args):

        quota_size = sizes[parsed_args.size]
        regions = parsed_args.regions
        services = parsed_args.services
        project = parsed_args.project

        if not services:
            output.print_red("Must supply services or use defaults.")
            return
        else:
            services = services.split(",")

        keystone = self.app.client_manager.identity

        try:
            project_obj = keystone.projects.get(project)
            project_id = project_obj.id
        except ks_exceptions.NotFound:
            projects = keystone.projects.list(name=project)
            if projects:
                project_obj = projects[0]
            else:
                raise ks_exceptions.NotFound(
                    "Project '%s' not found." % project)
            project_id = project_obj.id

        print(
            "Updating quota for project: %s (%s)" % (
                project_obj.name, project_obj.id))

        if regions:
            regions = regions.split(",")
            active_services = {r: services for r in regions}
        else:
            regions = [region.id for region in keystone.regions.list()]
            active_services = {"*": services}

        quota_manager = QuotaManager(
            self.app.client_manager.session, project_id,
            active_services=active_services)

        for region in regions:

            output.print_yellow("\nUpdating in region: %s" % region)

            region_data = quota_manager.get_region_quota_data(region)

            region_info = {
                'Region': region,
                'Current Quota Size': region_data['current_quota_size'],
                'New Quota Size': parsed_args.size,
            }
            output.print_dict(region_info)

            for service, service_quota in quota_size.items():
                quota_rows = []

                if service not in region_data['current_quota']:
                    continue

                for key in service_quota.keys():
                    quota_rows.append([
                        key,
                        service_quota[key],
                        region_data['current_quota'][service][key]])

                print("Quota changes in %s:" % service.title())
                output.print_list_rows(
                    self.find_differences(
                        region_data['current_quota'][service],
                        quota_size[service]),
                    ["Quota", "Old", "New"])

            if not parsed_args.no_op:
                quota_manager.set_region_quota(region, quota_size)
                print(
                    "Quota updated from %s to %s" %
                    (region_data['current_quota_size'], parsed_args.size))
            else:
                output.print_yellow(
                    "Would update quota from %s to %s" %
                    (region_data['current_quota_size'], parsed_args.size))


class ListQuotaSizes(command.Command):
    """
    Lists the the quota sizes or output as yaml.
    """

    def get_parser(self, prog_name):
        quota_args = super(ListQuotaSizes, self).get_parser(prog_name)

        quota_args.add_argument(
            '--yaml', action='store_true',
            help="Output as yaml.",
            default=False)

        return quota_args

    def take_action(self, parsed_args):
        if parsed_args.yaml:
            sizes_yaml = ""
            for size in sizes_ascending:
                sizes_yaml += yaml.dump(
                    {size: sizes[size]},
                    default_flow_style=False)
            print(sizes_yaml)
        else:
            ordered_sizes = OrderedDict()
            for size in sizes_ascending:
                ordered_sizes[size] = sizes[size]
            formatters = {
                k: output.json_formatter for k in ordered_sizes.keys()
            }
            output.print_dict(
                ordered_sizes, formatters=formatters, sortby=None)


class AlignQuotasToSize(command.Command):
    """
    For given project-ids or for ALL projects, try and find the nearest
    size and align the project to that size.

    If the project has usage above the nearest size, then go higher until
    a size that matches is found.
    """

    def get_parser(self, prog_name):
        quota_args = super(AlignQuotasToSize, self).get_parser(prog_name)

        quota_args.add_argument(
            '--project-id',
            nargs='*', default=[],
            help='List of project ids to align quotas for.')
        quota_args.add_argument(
            '--regions', metavar='<regions>',
            help='Comma separated list of regions. Defaults to all regions.')
        quota_args.add_argument(
            '--services', metavar='<services>',
            help='Comma separated list of services. Defaults to: '
                 '"cinder,nova,neutron,octavia"',
            default='cinder,nova,neutron,octavia')
        quota_args.add_argument(
            '--op', action='store_true',
            help="Perform operations. The default case for safety is no-op.",
            default=False)

        return quota_args

    def take_action(self, parsed_args):
        regions = parsed_args.regions
        services = parsed_args.services

        if not services:
            output.print_red("Must supply services or use defaults.")
            return
        else:
            services = services.split(",")

        keystone = self.app.client_manager.identity

        self.projects = []
        if parsed_args.project_id:
            self.projects = parsed_args.project_id

        if not self.projects:
            self.projects = [p.id for p in keystone.projects.list()]

        if regions:
            regions = regions.split(",")
            active_services = {r: services for r in regions}
        else:
            regions = [region.id for region in keystone.regions.list()]
            active_services = {"*": services}

        for project_id in self.projects:

            project = keystone.projects.get(project_id)
            proj_str = "%s (%s)" % (project.name, project.id)

            output.print_yellow("Processing project: %s" % proj_str)

            quota_manager = QuotaManager(
                self.app.client_manager.session, project.id,
                active_services=active_services)

            for region in regions:
                print("Processing region: %s" % region)

                region_data = quota_manager.get_region_quota_data(region)

                print("Current size is: %s" %
                      region_data['current_quota_size'])

                nearest_sizes = quota_manager.get_quota_differences(
                    region_data['current_quota'])

                nearest_sizes_ascending = sorted(
                    nearest_sizes, key=nearest_sizes.get)

                found_new_size = False
                for size in nearest_sizes_ascending:
                    if not quota_manager.is_usage_greater_than_size(
                            region, size, region_data['current_usage']):
                        if parsed_args.op:
                            quota_manager.set_region_quota(region, sizes[size])
                            print("Aligned quota to size '%s' for project %s" %
                                  (size, proj_str))
                        else:
                            print("Would aligned to size '%s' for project %s" %
                                  (size, proj_str))
                        found_new_size = True
                        break
                    else:
                        print("Skipping size %s as usage is higher." % size)

                if not found_new_size:
                    print("Failed to find valid quota size for project %s" %
                          proj_str)
