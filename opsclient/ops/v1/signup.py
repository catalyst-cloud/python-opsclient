# Copyright (c) 2017 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import json
import logging
from collections import OrderedDict
import sys
import urlparse

from osc_lib.command import command

from opsclient.common import prompt
from opsclient.common import output


LOG = logging.getLogger(__name__)


class SignUp(command.Command):
    """Signup Script to load signups in Adjutant.

    Will read in a json and submit it to Adjutant, then approve
    if the task is valid.
    """

    def get_parser(self, prog_name):
        # this super call gets the parser object
        parser = super(SignUp, self).get_parser(prog_name)

        parser.add_argument(
            'signup_json', metavar='<signup_json>',
            help="A json file containing the signup details.")
        return parser

    def take_action(self, parsed_args):
        with open(parsed_args.signup_json) as f:
            signup_data = json.load(f, object_pairs_hook=OrderedDict)

        output.print_dict(signup_data, sortby=False)
        if not prompt.prompt_yes_no("Is the above data correct?"):
            output.print_yellow("Exiting. Update your json file and rerun.")
            sys.exit()

        url = self.app.client_manager.get_endpoint_for_service_type(
            'registration')
        if not url.endswith('/'):
            url += '/'

        headers = {'Content-Type': 'application/json'}
        try:
            signup_url = urlparse.urljoin(url, "openstack/sign-up")
            resp = self.app.client_manager.session.request(
                signup_url, 'POST', data=json.dumps(signup_data),
                headers=headers).json()
        except Exception as e:
            output.print_red("Error while submitting task:")
            output.print_red(e.response.content)
            sys.exit()

        output.print_dict(resp)
        try:
            task_url = urlparse.urljoin(url, "tasks/" + resp['task'])
            resp = self.app.client_manager.session.request(
                task_url, 'GET').json()
        except Exception as e:
            output.print_red("Error while getting submitted task details:")
            output.print_red(e.response.content)
            sys.exit()

        formatters = {
            "action_notes": output.json_formatter,
            "actions": output.json_formatter,
            "keystone_user": output.json_formatter,
            "approved_by": output.json_formatter,
        }
        output.print_dict(resp, formatters=formatters)

        valid = all([a['valid'] for a in resp['actions']])
        if not valid:
            output.print_red("Task is not valid, cannot approve. Exiting.")
            sys.exit()

        prompt_question = (
            "After checking the above validation notes, are you certain you "
            "would you like to appove the this task?"
        )
        if prompt.prompt_safe(prompt_question):
            try:
                resp = self.app.client_manager.session.request(
                    task_url, 'POST',
                    data=json.dumps({'approved': True}),
                    headers=headers).json()
            except Exception as e:
                output.print_red("Error while approving task:")
                output.print_red(e.response.content)
                sys.exit()

            output.print_dict(resp)
            try:
                resp = self.app.client_manager.session.request(
                    task_url, 'GET').json()
            except Exception as e:
                output.print_red("Error while getting approved task details:")
                output.print_red(e.response.content)
                sys.exit()
