# Copyright (c) 2018 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from collections import defaultdict
import json

from keystoneauth1.exceptions.http import NotFound

from osc_lib.command import command


class RemoveRoleAssignments(command.Command):
    """
    From all projects, for all users, for all groups, remove all assignments
    of the given role.

    THIS IS VERY POWERFUL! Use wisely and carefully.
    """

    def get_parser(self, prog_name):
        parser = super(RemoveRoleAssignments, self).get_parser(prog_name)

        parser.add_argument(
            'role', metavar='<role>',
            help='Role to remove.')

        parser.add_argument(
            '--ignore-groups', action='store_true',
            help="Don't remove role assignments for groups",
            default=False
        )

        parser.add_argument(
            '--op', action='store_true',
            help="Perform operations. The default case for safety is no-op.",
            default=False
        )

        return parser

    def take_action(self, parsed_args):
        ks = self.app.client_manager.identity

        print("Fetching users...")
        users = ks.users.list()
        user_dict = {user.id: user for user in users}

        print("Fetching groups...")
        groups = ks.groups.list()
        group_dict = {group.id: group for group in groups}

        print("Fetching roles...")
        roles = ks.roles.list()
        role_dict = {role.name: role for role in roles}
        role_dict.update({role.id: role for role in roles})

        if parsed_args.role in role_dict:
            remove_role = role_dict[parsed_args.role]
        else:
            print("'%s' is not a valid role." % remove_role)
            return

        print("Fetching projects...")
        projects = ks.projects.list() + ks.projects.list(is_domain=True)
        project_dict = {project.id: project for project in projects}

        print("Fetching assignments...")
        assignments = ks.role_assignments.list(role=remove_role)

        actions_done = defaultdict(list)

        def add_note(key, msg):
            print(msg)
            actions_done[key].append(msg)

        try:
            for assignment in assignments:
                if getattr(assignment, 'group', None):
                    if parsed_args.ignore_groups:
                        continue
                    group = group_dict[assignment.group['id']]
                    note_key = "group: %s" % group.name
                    print("\nProcessing group: %s" % group.name)
                    user = None
                else:
                    user = user_dict[assignment.user['id']]
                    note_key = "user: %s" % user.name
                    print("\nProcessing user: %s" % user.name)
                    group = None

                if 'OS-INHERIT:inherited_to' in assignment.scope:
                    inherit = True
                else:
                    inherit = False

                try:
                    project = project_dict[assignment.scope['project']['id']]
                    domain = None
                    scope_str = "project: %s (%s)" % (project.name, project.id)
                except KeyError:
                    # Skip system scope for now
                    if 'system' in assignment.scope:
                        continue
                    domain = project_dict[assignment.scope['domain']['id']]
                    project = None
                    scope_str = "domain: %s (%s)" % (domain.name, domain.id)

                if parsed_args.op:
                    try:
                        ks.roles.revoke(
                            remove_role, user=user, group=group,
                            project=project, domain=domain,
                            os_inherit_extension_inherited=inherit)
                        add_note(
                            note_key,
                            "Revoked%s role: %s on %s" %
                            (' inherited' if inherit else '',
                             remove_role.name, scope_str)
                        )
                    except NotFound:
                        add_note(
                            note_key,
                            "Already revoked%s role: %s on %s" %
                            (' inherited' if inherit else '',
                             remove_role.name, scope_str)
                        )
                else:
                    add_note(
                        note_key,
                        "Will revoke%s role: %s on %s" %
                        (' inherited' if inherit else '',
                         remove_role.name, scope_str)
                    )
        except Exception as e:
            print("ERROR: %s '%s'" % (type(e), e))
            print("Stopping command.\n")
            raise
        finally:
            print("\nAction Notes:")
            print(json.dumps(actions_done, sort_keys=True,
                             indent=4, separators=(',', ': ')))


class ConditionalAddRole(command.Command):
    """
    This will grant a role on a given scope for users and groups where the
    users or groups meet a condition on that scope.

    This condition can be a specific role, or any role
    on a project or domain.

    Inherited roles on a project or domain are handled separate
    to roles assigned to a project/domain itself. If a user/group meets
    the condition on the project/domain normal/inherited then they will
    get the given role on normal/inherited where it is met.

    If any role on a project, you can then filter
    using exclude_roles so the condition becomes any role on a
    project as long as none of those are in exclude_roles.

    Example scenarios:
    - grant heat_stack_owner to all users and groups on projects where
      they have _member_
    - grant _member_ to all users and groups on projects where they have
      any role as long as they do not have admin on those projects.
    """

    def get_parser(self, prog_name):
        parser = super(ConditionalAddRole, self).get_parser(prog_name)

        group = parser.add_mutually_exclusive_group()

        group.add_argument(
            '--has-role', metavar='<has_role>',
            help=('Preconditional role, if left empty will go by '
                  '"any role on project".'),
            default="")

        group.add_argument(
            '--exclude-roles', metavar='<exclude_roles>',
            help=('Comma separated list of role names. '
                  'Exclude users and groups with these roles when '
                  '--has-role is None.'),
            required=False)

        parser.add_argument(
            '--give-role', metavar='<give_role>',
            help='Role to add if precondition is met.')

        parser.add_argument(
            '--ignore-inherited', action='store_true',
            help="Don't give inherited roles.",
            default=False
        )

        parser.add_argument(
            '--ignore-groups', action='store_true',
            help="Don't give role to groups.",
            default=False
        )

        parser.add_argument(
            '--op', action='store_true',
            help="Perform operations, default is perform no operations.",
            default=False
        )

        return parser

    def take_action(self, parsed_args):
        ks = self.app.client_manager.identity

        print("Fetching users...")
        users = ks.users.list()
        user_dict = {user.id: user for user in users}

        print("Fetching groups...")
        groups = ks.groups.list()
        group_dict = {group.id: group for group in groups}

        print("Fetching roles...")
        roles = ks.roles.list()
        role_dict = {role.name: role for role in roles}
        role_dict.update({role.id: role for role in roles})

        if parsed_args.has_role:
            if parsed_args.has_role in role_dict:
                has_role = role_dict[parsed_args.has_role]
            else:
                print("'%s' is not a valid role." % has_role)
                return
        else:
            has_role = False

        skip_set = set()
        if parsed_args.exclude_roles:
            for role in parsed_args.exclude_roles.split(","):
                if role in role_dict:
                    skip_set.add(role_dict[role].id)
                else:
                    print("'%s' is not a valid role." % role)
                    return

        if parsed_args.give_role in role_dict:
            give_role = role_dict[parsed_args.give_role]
        else:
            print("'%s' is not a valid role." % give_role)
            return

        print("Fetching projects...")
        projects = ks.projects.list() + ks.projects.list(is_domain=True)
        project_dict = {project.id: project for project in projects}

        print("Fetching assignments...")
        if has_role:
            assignments = ks.role_assignments.list(role=has_role)
        else:
            assignments = ks.role_assignments.list()

        print("Processing assignments...")
        users = defaultdict(dict)
        groups = defaultdict(dict)
        for assignment in assignments:
            try:
                scope = assignment.scope['project']['id']
                scope_key = 'project'
            except KeyError:
                # Skip system scope for now
                if 'system' in assignment.scope:
                    continue
                scope = assignment.scope['domain']['id']
                scope_key = 'domain'

            if 'OS-INHERIT:inherited_to' in assignment.scope:
                if parsed_args.ignore_inherited:
                    continue
                inherited = True
            else:
                inherited = False

            if getattr(assignment, 'user', False):
                user = user_dict[assignment.user['id']]

                try:
                    users[user.id][scope_key][scope].append(
                        {'role': role_dict[assignment.role["id"]],
                         'inherited': inherited}
                    )
                except KeyError:
                    users[user.id][scope_key] = defaultdict(list)
                    users[user.id][scope_key][scope].append(
                        {'role': role_dict[assignment.role["id"]],
                         'inherited': inherited}
                    )
            elif not parsed_args.ignore_groups:
                group = group_dict[assignment.group['id']]

                try:
                    groups[group.id][scope_key][scope].append(
                        {'role': role_dict[assignment.role["id"]],
                         'inherited': inherited}
                    )
                except KeyError:
                    groups[group.id][scope_key] = defaultdict(list)
                    groups[group.id][scope_key][scope].append(
                        {'role': role_dict[assignment.role["id"]],
                         'inherited': inherited}
                    )

        actions_done = defaultdict(list)

        def add_note(key, msg):
            print(msg)
            actions_done[key].append(msg)

        def process_actors(actors, actor_dict, actor_type):
            for actor, scopes in actors.items():
                actor_obj = actor_dict[actor]
                note_key = "%s: %s" % (actor_type, actor_obj.name)
                actor_kargs = {actor_type: actor_obj}

                print("\nProcessing %s: %s" % (actor_type, actor_obj.name))

                for scope, projects in scopes.items():
                    for project, roles in projects.items():
                        normal_roles = set([
                            r['role'].id for r in roles
                            if not r['inherited']])
                        inherited_roles = set([
                            r['role'].id for r in roles
                            if r['inherited']])

                        add_normal = len(normal_roles)
                        add_inherited = len(inherited_roles)
                        if not has_role and skip_set:
                            if normal_roles & skip_set:
                                print("Skipping role for %s." % actor_type)
                                add_normal = False

                            if inherited_roles & skip_set:
                                print("Skipping inherited role for %s."
                                      % actor_type)
                                add_inherited = False

                        if scope == 'domain':
                            domain_obj = project_dict[project]
                            project_obj = None
                            scope_str = "project: %s (%s)" % (
                                domain_obj.name, domain_obj.id)
                        else:
                            domain_obj = None
                            project_obj = project_dict[project]
                            scope_str = "domain: %s (%s)" % (
                                project_obj.name, project_obj.id)

                        if add_normal:
                            try:
                                has_new_normal = ks.roles.check(
                                    give_role, project=project_obj,
                                    domain=domain_obj, **actor_kargs)
                            except NotFound:
                                has_new_normal = False

                            if has_new_normal:
                                add_note(
                                    note_key,
                                    "Already has role: %s on %s" %
                                    (give_role.name, scope_str)
                                )
                            else:
                                if parsed_args.op:
                                    ks.roles.grant(
                                        give_role, project=project_obj,
                                        domain=domain_obj, **actor_kargs)
                                    add_note(
                                        note_key,
                                        "Granted role: %s on %s" %
                                        (give_role.name, scope_str)
                                    )
                                else:
                                    add_note(
                                        note_key,
                                        "Will grant role: %s on %s" %
                                        (give_role.name, scope_str)
                                    )

                        if add_inherited:
                            try:
                                has_new_inherited = ks.roles.check(
                                    give_role, project=project_obj,
                                    domain=domain_obj,
                                    os_inherit_extension_inherited=True,
                                    **actor_kargs)
                            except NotFound:
                                has_new_inherited = False

                            if has_new_inherited:
                                add_note(
                                    note_key,
                                    "Already has inherited role: %s "
                                    "on %s" %
                                    (give_role.name, scope_str)
                                )
                            else:
                                if not parsed_args.op:
                                    add_note(
                                        note_key,
                                        "Will grant inherited role: %s "
                                        "on %s" %
                                        (give_role.name, scope_str)
                                    )
                                    continue

                                ks.roles.grant(
                                    give_role, project=project_obj,
                                    domain=domain_obj,
                                    os_inherit_extension_inherited=True,
                                    **actor_kargs)
                                add_note(
                                    note_key,
                                    "Granted inherited role: %s "
                                    "on %s" %
                                    (give_role.name, scope_str)
                                )

        try:
            process_actors(users, user_dict, 'user')
            process_actors(groups, group_dict, 'group')
        except Exception as e:
            print("ERROR: %s '%s'" % (type(e), e))
            print("Stopping command.\n")
            raise
        finally:
            print("\nAction Notes:")
            print(json.dumps(actions_done, sort_keys=True,
                             indent=4, separators=(',', ': ')))
