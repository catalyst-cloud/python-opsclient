# Copyright (c) 2017 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from neutronclient.v2_0 import client as neutronclient
from novaclient import client as novaclient

from osc_lib.command import command

from opsclient.common import output


class IPAddressInfo(command.Command):
    """Display info regarding a public IP
    """

    def _floatingip_target_info(self, region, port_id):
        network_client = neutronclient.Client(
            session=self.app.client_manager.session,
            region_name=region)

        compute_client = novaclient.Client(
            2, session=self.app.client_manager.session,
            region_name=region)

        target_info = {}
        port = network_client.show_port(port_id)['port']

        security_groups = []
        for security_group_id in port['security_groups']:
            security_group = network_client.show_security_group(
                security_group_id)['security_group']
            security_groups.append(
                '%s %s' % (security_group['name'], security_group['id']))

        instance = compute_client.servers.get(port['device_id'])
        target_info['name'] = instance.name
        target_info['id'] = instance.id
        target_info['port_id'] = port['id']
        target_info['hypervisor'] = port['binding:host_id']

        tap_interface = 'tap' + port['id'][:11]
        target_info['ProTip'] = output.yellow(
            "tcpdump on the instance interface... "
            "'tcpdump -ni %s' on %s" % (tap_interface, port['binding:host_id'])
        )
        return target_info

    def _port_from_ip(self, region, ip):
        network_client = neutronclient.Client(
            session=self.app.client_manager.session,
            region_name=region)
        for port in network_client.list_ports()['ports']:
            for fixed_ip in port['fixed_ips']:
                if fixed_ip['ip_address'] == ip:
                    return port
        return None

    def _get_router_info(self, region, router_id):
        network_client = neutronclient.Client(
            session=self.app.client_manager.session,
            region_name=region)

        router_info = {}
        router = network_client.show_router(router_id)['router']
        router_info['id'] = router_id

        agents = network_client.list_l3_agent_hosting_routers(
            router_id)['agents']
        if not agents:
            router_info['l3_agent_error'] = output.red(
                "Router '%s' does not appear to be scheduled." % router['id']
            )
        elif len(agents) == 1:
            if agents[0]['alive']:
                agent_state = output.green('ALIVE')
            else:
                agent_state = output.red('DEAD')
            router_info['l3_agent_host'] = agents[0]['host']
            router_info['l3_agent_state'] = agent_state
        else:
            router_info['l3_agent_error'] = output.red(
                "Router '%s' is scheduled to multiple agents." % router['id']
            )

        router_info['ProTip'] = output.yellow(
            "tcpdump inside the router namespace... "
            "'ip netns exec qrouter-%s tcpdump -ln' on %s" % (
                router['id'], agents[0]['host'])
        )

        router_vpnservices = network_client.list_vpnservices(
            router_id=router_id)['vpnservices']
        router_info['VPN Services'] = []
        for vpnservice in router_vpnservices:
            vpn_dict = {
                'id': vpnservice['id'],
                'name': vpnservice['name'],
                'description': vpnservice['description'],
                'IPsec Site Connections': []
            }
            vpn_site_connections = network_client.list_ipsec_site_connections(
                vpnservice_id=vpnservice['id'])['ipsec_site_connections']
            for site_conn in vpn_site_connections:
                vpn_dict['IPsec Site Connections'].append({
                    'name': site_conn['name'],
                    'status': site_conn['status'],
                    'id': site_conn['id'],
                    'Remote Peer Address': site_conn['peer_address'],
                    'Remote Network(s)': site_conn['peer_cidrs'],
                })
            router_info['VPN Services'].append(vpn_dict)

        return router_info

    def get_parser(self, prog_name):
        parser = super(IPAddressInfo, self).get_parser(prog_name)
        parser.add_argument(
            'ip_address', metavar='<ip_address>',
            help='IP address to get info for.')
        parser.add_argument(
            '--regions', metavar='<regions>',
            help='Regions to look in. Comma separated list of regions. '
                 'Defaults to all regions.')

        return parser

    def take_action(self, parsed_args):
        regions = parsed_args.regions
        if regions:
            regions = regions.split(",")
        else:
            keystone = self.app.client_manager.identity
            regions = [region.id for region in keystone.regions.list()]

        ip_address = parsed_args.ip_address

        ip_object = None
        port_object = None
        found_region = None
        for region in regions:
            network_client = neutronclient.Client(
                session=self.app.client_manager.session,
                region_name=region)
            ip_objects = network_client.list_floatingips(
                floating_ip_address=ip_address)['floatingips']
            if ip_objects:
                ip_object = ip_objects[0]
                found_region = region
                break

            port_object = self._port_from_ip(region, ip_address)
            if port_object:
                found_region = region
                break

        if not ip_object and not port_object:
            # We haven't found the given ip address.
            output.print_yellow("IP not found.")
            return

        ip_info = {'ip address': ip_address}
        instance_info = {}
        router_info = {}

        identity_client = self.app.client_manager.identity
        if ip_object:
            ip_info['id'] = ip_object['id']

            project = identity_client.projects.get(ip_object['tenant_id'])
            region_project_info = {
                'region': found_region,
                'project': "%s (%s)" % (project.name, project.id)}
            ip_info.update(region_project_info)

            if ip_object['port_id']:
                ip_info['type'] = 'Floating IP'
                instance_info.update(self._floatingip_target_info(
                    found_region, ip_object['port_id']))
            else:
                ip_info['type'] = 'Unassociated Floating IP'

            ip_info['mapping'] = ('%s <-> %s' % (
                ip_address, ip_object['fixed_ip_address']))
            ip_info['port_id'] = ip_object['port_id']

            if ip_object['router_id']:
                router_info.update(self._get_router_info(
                    found_region, ip_object['router_id']))

        if port_object:
            if port_object['device_id']:
                ip_info['type'] = 'Gateway IP'
                router = network_client.show_router(
                    port_object['device_id'])['router']

                project = identity_client.projects.get(router['tenant_id'])
                region_project_info = {
                    'region': found_region,
                    'project': "%s (%s)" % (project.name, project.id)}
                ip_info.update(region_project_info)
                ip_info['port_id'] = port_object['id']

                router_info.update(self._get_router_info(
                    found_region, port_object['device_id']))
            else:
                ip_info['type'] = 'Unassociated IP'

        print("\nIP info:")
        output.print_dict(ip_info)

        if instance_info:
            print("\nInstance info:")
            instance_info.update(region_project_info)
            output.print_dict(instance_info)

        if router_info:
            print("\nRouter info:")
            router_info.update(region_project_info)
            output.print_dict(
                router_info,
                formatters={'VPN Services': output.json_formatter})
