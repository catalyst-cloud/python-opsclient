# Copyright (c) 2016 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from collections import namedtuple
import logging
import time

from osc_lib.command import command

from opsclient.common import profile
from opsclient.common import prompt
from opsclient.common import output


LOG = logging.getLogger(__name__)


class CommandExample(command.Command):
    """This is the help text for the command itself.

    This command inherits from "Command", which is a genric
    command that does nothing nor expects return data.

    This type is best used when you want to print multiple
    things, or you want control over the output formatting.

    In that case we're best to use the 'output' and 'prompt'
    modules to help control the flow and output of this.

    Chances are this is command type you'll use more often
    as it gives you the most control and doesn't expect anything.
    """

    def get_parser(self, prog_name):
        """This where the command arguments are defined.

        This is standard python argparse so it is easy enough to
        use and the docs for that can be found here:
        https://docs.python.org/3/library/argparse.html
        """
        # this super call gets the parser object
        parser = super(CommandExample, self).get_parser(prog_name)

        parser.add_argument(
            'positional', metavar='<positional>',
            help="A positional argument.")
        parser.add_argument(
            '--non-optional', metavar='<non_optional>',
            help='A required optional argument.')
        parser.add_argument(
            '--optional', metavar='<non_optional>',
            required=False,
            help='An optional argument.')
        parser.add_argument(
            '--flag', action='store_true',
            help='A basic boolean flag argument (default is false).')
        return parser

    def take_action(self, parsed_args):
        """The command action itself.

        This is where you put the code you want to run.

        Any arguments you defined will be present in 'parsed_args'.

        All openstack authentication via the OS envvars is handled
        elsewhere for you so don't do it yourself.

        For how to use the openstack clients see 'clients_example.py'.
        """

        # Your arguments:
        print("positional: %s" % parsed_args.positional)
        print("non_optional: %s" % parsed_args.non_optional)
        print("optional: %s" % parsed_args.optional)
        print("flag: %s" % parsed_args.flag)

        output.print_green(
            "As this is a 'Command' class, you will want to do "
            "your own output printing. The 'output' class has various"
            "ways for you to do that easily.")

        # You can colour output easily (blame Mucky for this):
        print("This text isn't red, %s" % output.red("but this text is."))

        some_dict = {
            "comment_1": "You can even print dicts in fancy tables.",
            "comment_2": output.yellow("And even colour the contents!"),
            "comment_as_json": {
                "comment_3": "And nicely format json in those tables!"},
        }
        formatters = {"comment_as_json": output.json_formatter}

        output.print_dict(some_dict, formatters=formatters)

        # you can even print a list of dicts, or a list of objects:
        dicts = [some_dict, ] * 3
        keys = ['comment_1', 'comment_2']
        # print a list of dicts and show selected keys:
        output.print_list(dicts, keys)

        # This is just me converting those dicts into objects for the
        # purposes of showing how 'print_list' is indifferent about
        # objects vs dicts:
        DictObject = namedtuple(
            'DictObject', 'comment_1 comment_2 comment_as_json')
        objects = [DictObject(**d) for d in dicts]
        fields = keys
        # print a list of objects and show selected fields:
        output.print_list(objects, fields)

        headers = ["type", "number", "content"]
        rows = [
            ["comment", 4, "You can even print basic lists of rows."],
            ["comment", 5, "This just uses prettytable directly."]
        ]
        output.print_list_rows(rows, headers)

        # The prompt is also useful:
        if prompt.prompt_yes_no("Do you want to print a skull?"):
            # And you have slightly safer prompt to make sure someone is
            # actually paying attention.
            if prompt.prompt_safe("Are you sure?"):
                print(output.SKULL)

        # Or maybe you want to see how long something takes:
        with profile.timed("'timed' context manager example"):
            time.sleep(2)
            print(
                "Anything put under this with statement will "
                "be timed.")
            time.sleep(2)
            print(
                "And on exiting the with statement, it will print "
                "the total time spent inside it.")
