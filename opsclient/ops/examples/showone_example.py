# Copyright (c) 2016 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from collections import OrderedDict
import logging
import six

from osc_lib.command import command


LOG = logging.getLogger(__name__)


class ShowOneExample(command.ShowOne):
    """This is the help text for the command itself.

    This command inherits from "ShowOne", which is useful
    for showing the details of one object or dict.
    """

    def get_parser(self, prog_name):
        """This where the command arguments are defined.

        This is standard python argparse so it is easy enough to
        use and the docs for that can be found here:
        https://docs.python.org/3/library/argparse.html
        """
        # this super call gets the parser object
        parser = super(ShowOneExample, self).get_parser(prog_name)

        parser.add_argument(
            'positional', metavar='<positional>',
            help="A positional argument.")
        parser.add_argument(
            '--non-optional', metavar='<non_optional>',
            help='A required optional argument.')
        parser.add_argument(
            '--optional', metavar='<non_optional>',
            required=False,
            help='An optional argument.')
        parser.add_argument(
            '--flag', action='store_true',
            help='A basic boolean flag argument (default is false).')
        return parser

    def take_action(self, parsed_args):
        """The command action itself.

        As this is a ShowOne, you have to return the data in a certain way:
        """

        fields = (
            'positional',
            'non_optional',
            'optional',
            'flag',
        )
        values = (
            parsed_args.positional,
            parsed_args.non_optional,
            parsed_args.optional,
            parsed_args.flag,
        )
        # Technically the above can be lists rather than tuples
        # but I'm making them tuples to show a point further down.

        to_return = [fields, values]

        # While we could return this now, let me show you another example.
        # If you can get your object as a dict you can then zip it.
        # I'm using ordered dict here so that the above order is preserved
        # but any dict will work.
        data = [
            ('positional', parsed_args.positional),
            ('non_optional', parsed_args.non_optional),
            ('optional', parsed_args.optional),
            ('flag', parsed_args.flag),
        ]
        args_dict = OrderedDict(data)
        # And now we just zip the dict into the same format as to_return
        to_return_2 = zip(*six.iteritems(args_dict))

        # you can even sort it by the keys if you want:
        key_sorted = zip(*sorted(six.iteritems(args_dict)))

        # because we've used the same order, these two should be the same
        # and key sorted will be different
        if to_return == to_return_2 and to_return != key_sorted:
            return to_return
