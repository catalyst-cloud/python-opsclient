# Copyright (c) 2016 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import logging

from osc_lib.command import command


LOG = logging.getLogger(__name__)


class ListerExample(command.Lister):
    """This is the help text for the command itself.

    This command inherits from "Lister", which is useful
    for listing losts of object details.
    """

    def get_parser(self, prog_name):
        """This where the command arguments are defined.

        This is standard python argparse so it is easy enough to
        use and the docs for that can be found here:
        https://docs.python.org/3/library/argparse.html
        """
        # this super call gets the parser object
        parser = super(ListerExample, self).get_parser(prog_name)

        parser.add_argument(
            'positional', metavar='<positional>',
            help="A positional argument.")
        parser.add_argument(
            '--non-optional', metavar='<non_optional>',
            help='A required optional argument.')
        parser.add_argument(
            '--optional', metavar='<non_optional>',
            required=False,
            help='An optional argument.')
        parser.add_argument(
            '--flag', action='store_true',
            help='A basic boolean flag argument (default is false).')
        return parser

    def take_action(self, parsed_args):
        """The command action itself.

        As this is a lister, you have to return the data in a certain way:
        """

        headers = ["argument", "input"]
        rows = [
            ["positional", parsed_args.positional],
            ["non_optional", parsed_args.non_optional],
            ["optional", parsed_args.optional],
            ["flag", parsed_args.flag],
        ]

        # Returned as a tuple:
        return headers, rows
