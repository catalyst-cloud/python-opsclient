# Copyright (c) 2016 Catalyst IT Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import logging

from cinderclient import client as cinderclient
from neutronclient.v2_0 import client as neutronclient
from novaclient import client as novaclient

from osc_lib.command import command

from opsclient.common import output


LOG = logging.getLogger(__name__)


class ClientsExample(command.Command):
    """This is the help text for the command itself.

    This example is just to show how to initialise the clients.
    """

    def get_parser(self, prog_name):
        """This where the command arguments are defined.

        This is standard python argparse so it is easy enough to
        use and the docs for that can be found here:
        https://docs.python.org/3/library/argparse.html
        """
        # this super call gets the parser object
        parser = super(ClientsExample, self).get_parser(prog_name)

        parser.add_argument(
            'positional', metavar='<positional>',
            help="A positional argument.")
        parser.add_argument(
            '--non-optional', metavar='<non_optional>',
            help='A required optional argument.')
        parser.add_argument(
            '--optional', metavar='<non_optional>',
            required=False,
            help='An optional argument.')
        parser.add_argument(
            '--flag', action='store_true',
            help='A basic boolean flag argument (default is false).')
        return parser

    def take_action(self, parsed_args):
        """The command action itself."""

        # keystone
        identity_client = self.app.client_manager.identity

        # lets get a list of all the projects I have a role
        # in.
        my_projects = identity_client.projects.list(
            user=self.app.client_manager.auth_ref.user_id)

        fields = ['name', 'id']
        output.print_list(my_projects, fields)

        # most of the time you can do:
        # self.app.client_manager.<service_type>
        # when doing things in one region, as it will default to
        # you OS_REGION.

        # nova
        compute_client = self.app.client_manager.compute

        # glance:
        image_client = self.app.client_manager.image

        # cinder
        volume_client = self.app.client_manager.volume

        # Neutron is a little more complicated as
        # self.app.client_manager.network != neutronclient
        # but rather the neutron openstackSDK.
        # self.app.client_manager.object_store also is not
        # swiftclient.

        # For neutron though you are best doing:
        network_client = neutronclient.Client(
            session=self.app.client_manager.session,
            region_name=self.app.client_manager.region_name)

        # If you do want to run code in ALL regions, this might be useful:
        regions = [region.id for region in identity_client.regions.list()]
        for region in regions:
            print("doing something in region: %s" % region)

            # although for this you will need to setup clients yourself
            # rather than the client_mananger, as changing region_name
            # on the client manager doesn't do anything (I'm fixing this
            # upstream).

            network_client = neutronclient.Client(
                session=self.app.client_manager.session,
                region_name=region)

            compute_client = novaclient.Client(
                2, session=self.app.client_manager.session,
                region_name=region)

            volume_client = cinderclient.Client(
                2, session=self.app.client_manager.session,
                region_name=region)
