import odoorpc

from opsclient.common.odoo.projects import CloudProjectManager
from opsclient.common.odoo.partners import PartnerManager
from .project_relationships import ProjectRelationshipManager


class OdooClient(object):
    """docstring for OdooClient"""

    def __init__(self, config):
        odoo_conf = config.get('odoo', {})
        self._odoo = odoorpc.ODOO(
            odoo_conf.get('hostname'),
            protocol=odoo_conf.get('protocol'),
            port=int(odoo_conf.get('port')),
            version=odoo_conf.get('version'))

        self._odoo.login(
            odoo_conf.get('database'),
            odoo_conf.get('user'),
            odoo_conf.get('password'))

        # TODO(adriant): Rename tenant to project once renamed in odoo:
        self._Project = self._odoo.env['cloud.tenant']
        self._Partner = self._odoo.env['res.partner']
        self._PartnerRelationship = self._odoo.env['cloud.tenant_partner']

        # Now setup the managers:
        self.projects = CloudProjectManager(self)
        self.partners = PartnerManager(self)
        self.project_relationships = ProjectRelationshipManager(self)
