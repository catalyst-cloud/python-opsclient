
class BaseManager(object):

    fields = None

    def _list_or_tuple(self, ids):
        if not isinstance(ids, list) or isinstance(ids, tuple):
            ids = [ids, ]
        return ids

    def get(self, ids, read=False):
        """Get one or more Resources by id.

        'ids' can be 1 id, or a list of ids.
        """
        if read:
            return self.resource_env.read(
                self._list_or_tuple(ids), fields=self.fields)
        return self.resource_env.browse(self._list_or_tuple(ids))

    def list(self, filters, get=True, read=False):
        """Get a list of Resources.

        'filters' is a list of search options.`
            [('field', '=', value), ]
        """
        ids = self.resource_env.search(filters)
        if get:
            return self.get(ids, read)
        else:
            return ids

    def create(self, **fields):
        """Create a Resource.

        'fields' is the dict of kwargs to pass to create.
        Allows slighly nicer syntax than having to pass in a dict.
        """
        return self.resource_env.create(fields)

    def load(self, fields, rows):
        """Loads in a Resource.

        'fields' is a list of fields to import. - list(str)
        'rows' is the item data. - list(list(str))
        """
        return self.resource_env.create(fields)

    def delete(self, ids):
        """Delete 1 or more Resources by id.

        'ids' can be 1 id, or a list of ids.

        returns True if deleted or not present.
        """
        return self.resource_env.unlink(self._list_or_tuple(ids))
