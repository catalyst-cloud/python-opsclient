# Copyright 2014 OpenStack Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import logging

from osc_lib import utils

LOG = logging.getLogger(__name__)

DEFAULT_OS_OPSCLIENT_VERSION = '1'
API_VERSION_OPTION = 'os_opsclient_version'
API_NAME = "ops"
API_VERSIONS = {
    "1": "opsclient.ops.v1.client.Client",
}


def make_client(instance):
    """Returns an queues service client."""
    version = instance._api_version[API_NAME]
    try:
        version = int(version)
    except ValueError:
        version = float(version)

    ops_client = utils.get_client_class(
        API_NAME,
        version,
        API_VERSIONS)

    LOG.debug('Instantiating ops client: %s', ops_client)

    return ops_client(
        instance.session,
        region=instance._region_name
    )


def build_option_parser(parser):
    """Hook to add global options."""
    parser.add_argument(
        '--os-opsclient-version',
        metavar='<opsclient-version>',
        default=utils.env(
            'OS_OPSCLIENT_VERSION',
            default=DEFAULT_OS_OPSCLIENT_VERSION),
        help=('Client version, default=' +
              DEFAULT_OS_OPSCLIENT_VERSION +
              ' (Env: OS_OPSCLIENT_VERSION)'))
    return parser
