*************************
Catalyst Cloud Ops Client
*************************

Welcome to the `Catalyst Cloud` Operation Client project!

**Table of Contents**

.. contents::
    :local:
    :depth: 2
    :backlinks: none

==================
Install / Updating
==================

Ensure you are in the environment you want the opsclient installed.

Run in the root directory of the repo::

    pip install .

Or install with the path to the directory::

    pip install openstack-opsclient/

When you want to upgrade the library, pull the latest code and redo the above
command.

===================
Development Install
===================

First install the requirements::

    pip install -r requirements.txt

If you want to be able to edit the code and have your commands
take into account those code changes right away you'll want
to install it with the develop command.

Install and use repo source (allows editing of code)::

    python setup.py develop


============
Contributing
============

How can I add a new command
---------------------------

See 'opsclient.ops.examples'

In fact, you can run the examples themselves as commands::

	openstack --os-opsclient-version 0 catalyst clients example <positional> --non-optional <non_positional>

The example commands are setup as version 0 of the ops client, so
you need to explicitly use that version to access them.

By using those examples as templates, create a new module in
'opsclient.ops.v1' for your new command. then add it as an additional
command in the 'setup.cfg' file.
